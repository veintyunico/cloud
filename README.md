/////////////////
Fichero 'README.md'.
...
Creacion de imagen.
```
$ git git@gitlab.com:veintyunico/cloud.git
$ cd cloud
$ sudo docker build -t cloud-docker .
```
Despliegue.
```
$ sudo docker run -d --name cloud-docker -p 9050:80 cloud-docker
```
//////////
